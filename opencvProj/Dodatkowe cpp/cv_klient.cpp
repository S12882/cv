//OcV Libs
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include <opencv2/face.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/objdetect/objdetect_c.h"

//Cuda Libs
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_devptrs.hpp"
#include "opencv2/core/cuda_types.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudalegacy.hpp"
#include "opencv2/core/ptr.inl.hpp"

//Basic
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>

//Socket Libs
#include <sys/types.h>
#include<WinSock2.h>
#include<Windows.h>
#include <ws2tcpip.h>
#define DEFAULT_BUFLEN 512
#define IMG_BUFLEN 1243897
#define DEFAULT_PORT "27015"  // the port users will be connecting to

#pragma comment (lib, "Ws2_32.lib")

//QT libs
#include "QtWidgets/QApplication"

    
using namespace std;
using namespace cv;

int server();
String face_cascade_name = "cascades/cuda/haarcascade_frontalface_alt.xml";
String profile_cascade_name = "cascades/cuda/haarcascade_profileface.xml";
String smile_cascade_name = "cascades/cuda/haarcascade_smile.xml";

Ptr<cuda::CascadeClassifier> face_cascade = cv::cuda::CascadeClassifier::create(face_cascade_name);
Ptr<cuda::CascadeClassifier> profile_cascade = cv::cuda::CascadeClassifier::create(profile_cascade_name);
Ptr<cuda::CascadeClassifier> smile_cascade = cv::cuda::CascadeClassifier::create(smile_cascade_name);

char* window_name = "Capture - Face detection";\
int pos_x;
int pos_y;
Mat mask_f, mask_left;
cuda::GpuMat g_mask, g_maskleft;
string text;
size_t i = 0;

int talker(Mat frame);
cuda::GpuMat putMask(cuda::GpuMat src,Point center,Size face_size, cuda::GpuMat maska);
cuda::GpuMat drawText(cuda::GpuMat& image, String text, Point& point);

vector<Rect> detect_faces(cuda::GpuMat& image);
vector<Rect> detect_profiles(cuda::GpuMat& image);
vector<Rect> detect_smile(cuda::GpuMat& image);

cuda::GpuMat putMask(cuda::GpuMat src, Point center, Size face_size, cuda::GpuMat maska)
{
    cuda::GpuMat mask1,src1;
    cuda::resize(maska,mask1,face_size);    
 
	if((center.y - face_size.width/2) > 0 && (center.x - face_size.width/2) > 0 &&  (center.x - face_size.width/2) < (face_size.width)+90){
    Rect roi(center.x - face_size.width/2, center.y - face_size.width/2, face_size.width, face_size.width);
    src(roi).copyTo(src1);
   
    cuda::GpuMat mask2,m,m1;
    cuda::cvtColor(mask1,mask2,CV_BGR2GRAY);
    cuda::threshold(mask2,mask2,230,255,CV_THRESH_BINARY_INV); 
    vector<cuda::GpuMat> maskChannels(3),result_mask(3);
    split(mask1, maskChannels);
    cuda::bitwise_and(maskChannels[0],mask2,result_mask[0]);
    cuda::bitwise_and(maskChannels[1],mask2,result_mask[1]);
    cuda::bitwise_and(maskChannels[2],mask2,result_mask[2]);
    merge(result_mask,m );         
 
	Mat mask3(mask2);
    mask3 = 255 - mask3;
	mask2.upload(mask3);
	
    vector<cuda::GpuMat> srcChannels(3);
    split(src1, srcChannels);
    cuda::bitwise_and(srcChannels[0],mask2,result_mask[0]);
    cuda::bitwise_and(srcChannels[1],mask2,result_mask[1]);
    cuda::bitwise_and(srcChannels[2],mask2,result_mask[2]);
    merge(result_mask,m1 );       
 
    cuda::addWeighted(m,1,m1,1,0,m1);     
    m1.copyTo(src(roi));
 
    return src;
	}else{
	return src;
	}
}

cuda::GpuMat drawText(cuda::GpuMat& image, String text, Point& point){

	Mat txt(image);
	cuda::GpuMat g_frame;
	putText(txt, text, point, FONT_HERSHEY_PLAIN, 2.0, CV_RGB(255,255,255), 1); 	
	g_frame.upload(txt);
	return g_frame;

}

vector<Rect> detect_faces(cuda::GpuMat& image)
{
	   vector<Rect> faces;
       cuda::GpuMat im(image.size(),CV_8UC1);
       cuda::GpuMat facesBuf_gpu;
       cuda::GpuMat frame_gray;

	     if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY);
        }
        else
        {
                image.copyTo(im);
        }

   
	face_cascade->detectMultiScale(im, facesBuf_gpu);
	face_cascade->convert(facesBuf_gpu, faces);
	
        return faces;		
 }

vector<Rect> detect_smile(cuda::GpuMat& image)
{
	   vector<Rect> smiles;
       cuda::GpuMat im(image.size(),CV_8UC1);
       cuda::GpuMat smileBuf_gpu;
  
	     if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY);
        }
        else
        {
                image.copyTo(im);
        }

   
	face_cascade->detectMultiScale(im, smileBuf_gpu);
	face_cascade->convert(smileBuf_gpu, smiles);
	
        return smiles;		
 }
  
vector<Rect> detect_profiles(cuda::GpuMat& image)
{
	   vector<Rect> profiles;
       cuda::GpuMat im(image.size(),CV_8UC1);
	   cuda::GpuMat profileBuf_gpu;
       cuda::GpuMat frame_gray;

           if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY);
        }
        else
        {
                image.copyTo(im);
        }

    profile_cascade->detectMultiScale(im, profileBuf_gpu);
	profile_cascade->convert(profileBuf_gpu, profiles);
	
        return profiles;
}


int main( void )
{   
    VideoCapture cap(0);
    Mat frame, frame2, frame_gray;
	cuda::GpuMat g_frame, g_mask, g_maskleft;
	
WSADATA wsaData;
int iResult;

SOCKET ListenSocket = INVALID_SOCKET;
SOCKET ClientSocket = INVALID_SOCKET;

struct addrinfo *result = NULL;
struct addrinfo hints;
struct sockaddr cli_addr;

int iSendResult;
char recvbuf[DEFAULT_BUFLEN];
unsigned char * imgbuf;
int recvbuflen = DEFAULT_BUFLEN;

 mask_f=imread("avatars/anon.jpg");
 mask_left=imread("avatars/anon_p.jpg");
 text="Who are You?"; 

g_mask.upload(mask_f);
g_maskleft.upload(mask_left);
	
     if (!cap.isOpened())  // if not success, exit program
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }

   double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
   double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

    cout << "Frame size: " << dWidth << " x " << dHeight << endl;

    namedWindow("MyVideo",CV_WINDOW_AUTOSIZE);


// Initialize Winsock
iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
if (iResult != 0) {
    printf("WSAStartup failed with error: %d\n", iResult);
    return 1;
}

ZeroMemory(&hints, sizeof(hints));
hints.ai_family = AF_INET;
hints.ai_socktype = SOCK_STREAM;
hints.ai_protocol = IPPROTO_TCP;
hints.ai_flags = AI_PASSIVE;

// Resolve the server address and port
iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
if ( iResult != 0 ) {
    printf("getaddrinfo failed with error: %d\n", iResult);
    WSACleanup();
    return 1;
}

// Create a SOCKET for connecting to server
ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
if (ListenSocket == INVALID_SOCKET) {
    printf("socket failed with error: %ld\n", WSAGetLastError());
    freeaddrinfo(result);
    WSACleanup();
    return 1;
}

// Setup the TCP listening socket
iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
if (iResult == SOCKET_ERROR) {
    printf("bind failed with error: %d\n", WSAGetLastError());
    freeaddrinfo(result);
    closesocket(ListenSocket);
    WSACleanup();
    return 1;
}

freeaddrinfo(result);


iResult = listen(ListenSocket, SOMAXCONN);
if (iResult == SOCKET_ERROR) {
    printf("listen failed with error: %d\n", WSAGetLastError());
    closesocket(ListenSocket);
    WSACleanup();
    return 1;
}


server();

// Accept a client socket
ClientSocket = accept(ListenSocket, NULL, NULL);
if (ClientSocket == INVALID_SOCKET) {
    printf("accept failed with error: %d\n", WSAGetLastError());
    closesocket(ListenSocket);
    WSACleanup();
    return 1;
}

	  vector<Rect> face;
	  vector<Rect> smile;
	  vector<Rect> profile;
	  
  for (;;i++){

   bool bSuccess = cap.read(frame); 

         if (!bSuccess) 
        {
             cout << "Cannot read a frame from video stream" << endl;
             break;
        }


g_frame.upload(frame);

if(profile.size()==0)
	{
    if (i % 3 == 0) 
	{
		face=detect_faces(g_frame);

			  for (size_t i = 0; i < face.size(); i++) 
	         {
				 
				  smile=detect_smile(g_frame);  

                               	  Point center(face[i].x + face[i].width/2, (face[i].y + face[i].height/2)-25 );
                                  g_frame=putMask(g_frame,center,Size((face[i].width)+50, (face[i].height)+50), g_mask); 
								  pos_x = max(face[i].tl().x - 20, 0);
                                  pos_y = max(face[i].tl().y - 20, 0);
								  g_frame=drawText(g_frame, text, Point(pos_x, pos_y));								  							 

						for( size_t j = 0; j < smile.size(); j++ )
                                  { //-- Draw the smail
                                      Point smile_center( face[i].x + smile[j].x + smile[j].width/2, face[i].y + smile[j].y + smile[j].height/2 );
                                      int radius = cvRound( (smile[j].width + smile[j].height)*0.25 );
                                      circle(frame, smile_center, radius, Scalar(255, 0, 255 ), 3, 8, 0 );
                                 }		  
	      }
		
		  }else{
      for (size_t i = 0; i < face.size(); i++) 
	  {
	   Point center( face[i].x + face[i].width/2, (face[i].y + face[i].height/2)-25 );
       g_frame=putMask(g_frame,center,Size((face[i].width)+50, (face[i].height)+50), g_mask);
	   g_frame=drawText(g_frame, text, Point(pos_x, pos_y));	
	  
	  } 	
   } 
}
		 
	if(face.size()==0)
	{		
	 if (i % 3 == 0) 
	 {	
		 profile=detect_profiles(g_frame);

			  for (size_t z = 0; z < profile.size(); z++) 
	         {                    
                               	 Point center(profile[z].x + profile[z].width/2, (profile[z].y + profile[z].height/2)-25 );
                                  g_frame=putMask(g_frame,center,Size((profile[z].width)+50, (profile[z].height)+50), g_maskleft);   
								  pos_x = max(profile[z].tl().x - 20, 0);
                                  pos_y = max(profile[z].tl().y - 20, 0);
								  g_frame=drawText(g_frame, text, Point(pos_x, pos_y));	
			  }
		
 }else{
       for (size_t z = 0; z < profile.size(); z++) 
	   {
	   Point center( profile[z].x + profile[z].width/2, (profile[z].y +profile[z].height/2)-25 );
       g_frame=putMask(g_frame,center,Size((profile[z].width)+50, (profile[z].height)+50), g_maskleft);
	   g_frame=drawText(g_frame, text, Point(pos_x, pos_y));	
	   } 	
    }
}
	Mat show(g_frame);
	imshow(window_name, show);
	
 iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
    if (iResult > 0) {
        printf("SERVER - bytes: %d\n", iResult);
        char * temp = (char *) malloc(9); memcpy(temp,recvbuf,8);
        if (strcmp(recvbuf, "ASK"))
        {   
            int retTamImg;                          
            int imgSize = 0;
       
            //---------------- SEND IMAGE ----------------//
            if (show.data != NULL)
            {               
                retTamImg = show.total() * show.elemSize();
                iSendResult = send( ClientSocket, (const char *) show.data, retTamImg, 0 );

                if (iSendResult == SOCKET_ERROR) {
                    printf("resposta de pedido de face falhou. erro: %d\n", WSAGetLastError());
                    closesocket(ClientSocket);
                    WSACleanup();
                    return 1;
                }
                else
                    printf("resposta de pedido de face enviada com suceso. Bytes: %d\n", iSendResult);

            }
            
        }
    }
    else if (iResult == 0)
        printf("Connection closing...\n");
    else  {
        printf("recv failed with error: %d\n", WSAGetLastError());
        closesocket(ClientSocket);
        WSACleanup();
        getchar();
        return 1;
    }


unsigned char key = waitKey(30);
	
  if(key =='1'){
 mask_f=imread("avatars/caps.jpg");
 mask_left=imread("avatars/profile_left.jpg");
 text="Captain!"; 
  }
  else if(key =='2'){
 mask_f=imread("avatars/Iron_Man.jpg");
 mask_left=imread("avatars/Iron_man_p.jpg");
 text="Mr.Stark"; 
  } 

g_mask.upload(mask_f);
g_maskleft.upload(mask_left);
     
          if (key == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
       {
            cout << "esc key is pressed by user" << endl;
            break; 
       }
	}

	// shutdown the connection since we're done
iResult = shutdown(ClientSocket, SD_SEND);
if (iResult == SOCKET_ERROR) {
    printf("shutdown failed with error: %d\n", WSAGetLastError());
    closesocket(ClientSocket);
    WSACleanup();
    getchar();
    return 1;
}

// cleanup
closesocket(ClientSocket);
WSACleanup();

return 0;
}


