#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include <opencv2/face.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

String face_cascade_name = "haarcascade_frontalface_alt.xml";
String profile_cascade_name = "haarcascade_profileface.xml";
String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";

CascadeClassifier face_cascade;
CascadeClassifier profile_cascade;
CascadeClassifier eyes_cascade;

char* window_name = "Capture - Face detection";\
IplImage* mask = 0;
Mat maska;
int pos_x;
int pos_y;
string text;
size_t i = 0;

Mat putMask(Mat src,Point center,Size face_size, VideoCapture cap, Mat maska);

Mat putMask(Mat src, Point center, Size face_size, VideoCapture cap, Mat maska)
{
    Mat mask1,src1;
   double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); 
   double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); 
   resize(maska,mask1,face_size);    
 
	if((center.y - face_size.width/2) > 0 && (center.x - face_size.width/2) > 0 &&  (center.x - face_size.width/2) < (face_size.width)+125){
    Rect roi(center.x - face_size.width/2, center.y - face_size.width/2, face_size.width, face_size.width);
    src(roi).copyTo(src1);
   
    Mat mask2,m,m1;
    cvtColor(mask1,mask2,CV_BGR2GRAY);
    threshold(mask2,mask2,230,255,CV_THRESH_BINARY_INV); 
    vector<Mat> maskChannels(3),result_mask(3);
    split(mask1, maskChannels);
    bitwise_and(maskChannels[0],mask2,result_mask[0]);
    bitwise_and(maskChannels[1],mask2,result_mask[1]);
    bitwise_and(maskChannels[2],mask2,result_mask[2]);
    merge(result_mask,m );         
 
    mask2 = 255 - mask2;
    vector<Mat> srcChannels(3);
    split(src1, srcChannels);
    bitwise_and(srcChannels[0],mask2,result_mask[0]);
    bitwise_and(srcChannels[1],mask2,result_mask[1]);
    bitwise_and(srcChannels[2],mask2,result_mask[2]);
    merge(result_mask,m1 );       
 
    addWeighted(m,1,m1,1,0,m1);     
    m1.copyTo(src(roi));
 
    return src;
	}else{
	return src;
	}
}

int main( void )
{
    VideoCapture cap(0);
    Mat frame, frame_gray, frame2;

	Mat mask_f=imread("avatars/caps.jpg");
	Mat mask_left=imread("avatars/profile_left.jpg");
	text = "Captain!";

    if( !face_cascade.load( face_cascade_name ) ){
		printf("--(!)Error loading face cascade\n"); return -1; 
	};

	if( !profile_cascade.load( profile_cascade_name ) ){
		printf("--(!)Error loading face cascade\n"); return -1; 
	};

    if( !eyes_cascade.load( eyes_cascade_name ) ){ 
		printf("--(!)Error loading eyes cascade\n"); return -1; 
	};

    

     if (!cap.isOpened())  // if not success, exit program
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }

   double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
   double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

    cout << "Frame size : " << dWidth << " x " << dHeight << endl;

    namedWindow("MyVideo",CV_WINDOW_AUTOSIZE); 

	  vector<Rect> faces;
	  vector<Rect> profiles;
	  vector<Rect> eyes;

  for (;;i++) {

   bool bSuccess = cap.read(frame); 

         if (!bSuccess) 
        {
             cout << "Cannot read a frame from video stream" << endl;
             break;
        }
    // Don't call detectFace on every iteration, it's too expensive
 if(profiles.size()==0){
    if (i % 6 == 0) {
     
    cvtColor(frame, frame_gray, CV_BGR2GRAY );
	equalizeHist(frame_gray, frame_gray);

    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0, Size(80, 80));
	

    for( size_t i = 0; i < faces.size(); i++ )
    {
        Mat faceROI = frame_gray( faces[i] );
		
       
        if( eyes.size() == 2)
        {
       
            Point center( faces[i].x + faces[i].width/2, (faces[i].y + faces[i].height/2)-25 );
            frame=putMask(frame,center,Size((faces[i].width)+50, (faces[i].height)+50), cap, mask_f);
			Mat face_i = Mat(frame, faces[i]); 
			cvtColor(face_i, face_i, cv::COLOR_BGR2GRAY); 
			resize(face_i, face_i, Size(100, 100), 0, 0, CV_INTER_NN); 
	
      
      putText( frame, text, cv::Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 1);
  
          
		}
	}

		  }else {
      for (size_t i = 0; i < faces.size(); i++) {
	   Point center( faces[i].x + faces[i].width/2, (faces[i].y + faces[i].height/2)-25 );
       frame=putMask(frame,center,Size((faces[i].width)+50, (faces[i].height)+50), cap, mask_f);
	   pos_x = max(faces[i].tl().x - 20, 0);
       pos_y = max(faces[i].tl().y - 20, 0);
	   putText( frame, text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 1); 
	  } 	
	} 
}

	if(faces.size()==0){
	 if (i % 6 == 0) {
		profile_cascade.detectMultiScale(frame_gray, profiles, 1.1, 2, 0, Size(80, 80));

	for( size_t z = 0; z < profiles.size(); z++)
    {
            Point center(profiles[z].x + profiles[z].width/2, (profiles[z].y + profiles[z].height/2)-25 );
            frame=putMask(frame,center,Size((profiles[z].width)+50, (profiles[z].height)+50), cap, mask_left);
            text = "My gosh, you look familiar...";
            putText( frame, text, Point(pos_x, pos_y), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 1);	
	}
	 }else{
       for (size_t z = 0; z < profiles.size(); z++) {
	   Point center( profiles[z].x + profiles[z].width/2, (profiles[z].y +profiles[z].height/2)-25 );
       frame=putMask(frame,center,Size((profiles[z].width)+50, (profiles[z].height)+50), cap, mask_left);
	   pos_x = max(profiles[z].tl().x - 20, 0);
       pos_y = max(profiles[z].tl().y - 20, 0);
	   putText( frame, text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 1); 
	  } 	
	 }
	}
  

    imshow( window_name, frame );
     
          if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
       {
            cout << "esc key is pressed by user" << endl;
            break; 
       }
	}
return 0;
}

