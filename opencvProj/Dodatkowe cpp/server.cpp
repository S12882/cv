#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include <opencv2/face.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/objdetect/objdetect_c.h"

//Cuda Libs
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_devptrs.hpp"
#include "opencv2/core/cuda_types.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudalegacy.hpp"
#include "opencv2/core/ptr.inl.hpp"

#include <iostream>

//Socket Libs 
#include <sys/types.h>
#include<WinSock2.h>
#include<Windows.h>
#include <ws2tcpip.h>

#define DEFAULT_BUFLEN 2048
#define IMG_BUFLEN 1243897
#define DEFAULT_PORT "27015"



using namespace cv;
using namespace std;

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int server()
{
	struct WSAData;

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
struct addrinfo *result = NULL,
                *ptr = NULL,
                hints;
char sendbuf [DEFAULT_BUFLEN];
char recvbuf[DEFAULT_BUFLEN];
char * imgbuf;
int iResult;
int recvbuflen = DEFAULT_BUFLEN;

    // Initialize Winsock
iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
if (iResult != 0) {
    printf("WSAStartup failed with error: %d\n", iResult);
     waitKey();
    return 1;
}

ZeroMemory( &hints, sizeof(hints) );
hints.ai_family = AF_UNSPEC;
hints.ai_socktype = SOCK_STREAM;
hints.ai_protocol = IPPROTO_TCP;

// Resolve the server address and port
iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
if ( iResult != 0 ) {
    printf("getaddrinfo failed with error: %d\n", iResult);
    WSACleanup();
    return 1;
}

// Attempt to connect to an address until one succeeds
for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

    // Create a SOCKET for connecting to server
    ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
        ptr->ai_protocol);
    if (ConnectSocket == INVALID_SOCKET) {
        printf("socket failed with error: %ld\n", WSAGetLastError());
        WSACleanup();
         waitKey();
        return 1;
    }

    // Connect to server.
    iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
		printf("Socket erro! %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        ConnectSocket = INVALID_SOCKET;
        continue;
    }
    break;
}

freeaddrinfo(result);

if (ConnectSocket == INVALID_SOCKET) {
    printf("Unable to connect to server! %d\n", WSAGetLastError());
    WSACleanup();
     waitKey();
    return 1;
}

//----------------- ASK FOR THE IMAGE ----------------------//

    strncpy(sendbuf, "ASK1111", 12); 

    // Send an initial buffer
    iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send ASK FACE failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        waitKey();
        return 1;
    }

    printf("ASK FACE SENT Bytes Sent: %ld\n", iResult);

    Mat  img = Mat::zeros(640,480, CV_8UC3);
    int  imgSize = img.total()*img.elemSize();
    char *sockData;
    sockData = (char *) malloc (sizeof(char) * imgSize + 2);
	

   for (int i = 0; i < imgSize; i += iResult) 
   {
	   printf("ok");
       iResult = recv(ConnectSocket, sockData +i, imgSize  - i, 0);
	    printf("ok");
       if ( iResult > 0 )
                printf("CLIENT - retorno do ASK FACE (%d/%d)- Bytes received: %d\n", i, imgSize, iResult);

        else if ( iResult == 0 )
            printf("CLIENT - retorno do ASK FACE - Connection closed\n");
        else
            printf("CLIENT - retorno do ASK FACE - error: %d\n", WSAGetLastError());

   }

     // Assign pixel value to img
     int iptr=0;
     for (int i = 0;  i < img.rows; i++) {
          for (int j = 0; j < img.cols; j++)    {                                     
               img.at<cv::Vec3b>(i,j) = Vec3b(sockData[iptr+ 0],sockData[iptr+1],sockData[iptr+2]);
               iptr=iptr+3;
           }
      }

	 namedWindow("outputCapture", 1);
     imshow("outputCapture", img);

}