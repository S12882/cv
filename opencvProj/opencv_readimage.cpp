﻿//CV
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include <opencv2/face.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/objdetect/objdetect_c.h"

//Cuda
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_devptrs.hpp"
#include "opencv2/core/cuda_types.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudalegacy.hpp"
#include "opencv2/core/ptr.inl.hpp"

//Basic
#include <iostream>
#include <stdio.h>
#include <iomanip>

//Qt
#include "QtWidgets/QApplication"

    
using namespace std;
using namespace cv;

//Cascades
String face_cascade_name = "cascades/cuda/haarcascade_frontalface_alt.xml";
String profile_cascade_name = "cascades/cuda/haarcascade_profileface.xml";
String smile_cascade_name = "cascades/cuda/haarcascade_smile.xml";

Ptr<cuda::CascadeClassifier> face_cascade = cv::cuda::CascadeClassifier::create(face_cascade_name);
Ptr<cuda::CascadeClassifier> profile_cascade = cv::cuda::CascadeClassifier::create(profile_cascade_name);
Ptr<cuda::CascadeClassifier> smile_cascade = cv::cuda::CascadeClassifier::create(smile_cascade_name);

//Global Vars
char* window_name = "Capture - Face detection";\
int pos_x;
int pos_y;
string text;
size_t i = 0;

//Funkcje
cuda::GpuMat putMask(cuda::GpuMat src,Point center,Size face_size, cuda::GpuMat maska);
cuda::GpuMat drawText(cuda::GpuMat& image, String text, Point& point);
cuda::GpuMat Draw(vector<Rect> vector, cuda::GpuMat g_frame, cuda::GpuMat g_mask);

vector<Rect> detect(cuda::GpuMat& image, vector<Rect> vector, Ptr<cuda::CascadeClassifier> cascade);

//Delete
vector<Rect> detect_faces(cuda::GpuMat& image);
vector<Rect> detect_profiles(cuda::GpuMat& image);
vector<Rect> detect_smile(cuda::GpuMat& image);

cuda::GpuMat putMask(cuda::GpuMat src, Point center, Size face_size, cuda::GpuMat maska)
{
    cuda::GpuMat mask1,src1;
    cuda::resize(maska,mask1,face_size);    
 
	if((center.y - face_size.width/2) > 0 && (center.x - face_size.width/2) > 0 &&  (center.x - face_size.width/2) < (face_size.width)+90){
    Rect roi(center.x - face_size.width/2, center.y - face_size.width/2, face_size.width, face_size.width);
    src(roi).copyTo(src1);
   
    cuda::GpuMat mask2,m,m1;
    cuda::cvtColor(mask1,mask2,CV_BGR2GRAY);
    cuda::threshold(mask2,mask2,230,255,CV_THRESH_BINARY_INV); 
    vector<cuda::GpuMat> maskChannels(3),result_mask(3);
    split(mask1, maskChannels);
    cuda::bitwise_and(maskChannels[0],mask2,result_mask[0]);
    cuda::bitwise_and(maskChannels[1],mask2,result_mask[1]);
    cuda::bitwise_and(maskChannels[2],mask2,result_mask[2]);
    merge(result_mask,m);         
 
	Mat mask3(mask2);
    mask3 = 255 - mask3;
	mask2.upload(mask3);
	
    vector<cuda::GpuMat> srcChannels(3);
    split(src1, srcChannels);
    cuda::bitwise_and(srcChannels[0],mask2,result_mask[0]);
    cuda::bitwise_and(srcChannels[1],mask2,result_mask[1]);
    cuda::bitwise_and(srcChannels[2],mask2,result_mask[2]);
    merge(result_mask,m1 );       
 
    cuda::addWeighted(m,1,m1,1,0,m1);     
    m1.copyTo(src(roi));
 
    return src;
	}else{
	return src;
	}
}

cuda::GpuMat drawText(cuda::GpuMat& image, String text, Point& point){

	Mat txt(image);
	cuda::GpuMat g_frame;
	putText(txt, text, point, FONT_HERSHEY_PLAIN, 2.0, CV_RGB(255,255,255), 1); 	
	g_frame.upload(txt);
	return g_frame;

}


cuda::GpuMat Draw(vector<Rect> vector, cuda::GpuMat g_frame, cuda::GpuMat g_mask){

	  for (size_t i = 0; i < vector.size(); i++) 
	         {
	          Point center(vector[i].x + vector[i].width/2, (vector[i].y + vector[i].height/2)-25 );
               g_frame=putMask(g_frame,center,Size((vector[i].width)+50, (vector[i].height)+50), g_mask); 
				pos_x = max(vector[i].tl().x - 20, 0);
                    pos_y = max(vector[i].tl().y - 20, 0);
						 g_frame=drawText(g_frame, text, Point(pos_x, pos_y));	
	        }
	  return g_frame;
}

vector<Rect> detect(cuda::GpuMat& image, vector<Rect> vector, Ptr<cuda::CascadeClassifier> cascade)
{	
       cuda::GpuMat im(image.size(),CV_8UC1);
       cuda::GpuMat facesBuf_gpu;
       cuda::GpuMat frame_gray;

	     if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY); // make gray
        }
        else
        {
                image.copyTo(im);
        }

   
	cascade->detectMultiScale(im, facesBuf_gpu);
	cascade->convert(facesBuf_gpu, vector);
	
        return vector;		
 }

// To delete
vector<Rect> detect_faces(cuda::GpuMat& image) // To delete
{
	   vector<Rect> faces;
       cuda::GpuMat im(image.size(),CV_8UC1);
       cuda::GpuMat facesBuf_gpu;
       cuda::GpuMat frame_gray;

	     if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY); // make gray
        }
        else
        {
                image.copyTo(im);
        }

   
	face_cascade->detectMultiScale(im, facesBuf_gpu);
	face_cascade->convert(facesBuf_gpu, faces);
	
        return faces;		
 } 
// To delete
vector<Rect> detect_smile(cuda::GpuMat& image) 
{
	   vector<Rect> smiles;
       cuda::GpuMat im(image.size(),CV_8UC1);
       cuda::GpuMat smileBuf_gpu;
  
	     if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY);
        }
        else
        {
                image.copyTo(im);
        }

   
	face_cascade->detectMultiScale(im, smileBuf_gpu);
	face_cascade->convert(smileBuf_gpu, smiles);
	
        return smiles;		
 }
// To delete  
vector<Rect> detect_profiles(cuda::GpuMat& image)
{
	   vector<Rect> profiles;
       cuda::GpuMat im(image.size(),CV_8UC1);
	   cuda::GpuMat profileBuf_gpu;
       cuda::GpuMat frame_gray;

           if(image.channels()==3)
        {
                cuda::cvtColor(image,im,CV_BGR2GRAY);
        }
        else
        {
                image.copyTo(im);
        }

    profile_cascade->detectMultiScale(im, profileBuf_gpu);
	profile_cascade->convert(profileBuf_gpu, profiles);
	
        return profiles;
}

 
int main( void )
{
    VideoCapture cap(0);
    Mat frame, mask_f, mask_left, mask_smile;
	cuda::GpuMat g_frame, g_mask, g_maskleft, g_mask_smile;

	//Default mask
mask_f=imread("avatars/anon.jpg");  
mask_left=imread("avatars/anon_p.jpg");
mask_smile=imread("avatars/smile.jpg");
text="Who are You?"; 

 //to gpu
g_mask.upload(mask_f);
g_maskleft.upload(mask_left);
g_mask_smile.upload(mask_smile);
	
     if (!cap.isOpened())  // if not success, exit program
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }
	
   double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
   double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

    cout << "Frame size: " << dWidth << " x " << dHeight << endl;

	  vector<Rect> face;
	  vector<Rect> smile;
	  vector<Rect> profile;

//cap loop
 for (;;i++){

   bool bSuccess = cap.read(frame); 

         if (!bSuccess) 
        {
             cout << "Cannot read a frame from video stream" << endl;
             break;
        }


g_frame.upload(frame); //to gpu
cuda::resize(g_frame, g_frame, Size(800, 600), 0, 0, INTER_CUBIC);

 if (i % 2 == 0) // perfomance feature
	{

if(profile.size()==0)
	{
  
		face=detect(g_frame, face, face_cascade);
			g_frame=Draw(face, g_frame, g_mask);
}
	 
if(face.size()==0)
	{		
		
		 profile=detect(g_frame, profile, profile_cascade);
		     g_frame=Draw(profile, g_frame, g_maskleft);		
}
		
 }else{
	
	g_frame=Draw(face, g_frame, g_mask);
    g_frame=Draw(profile, g_frame, g_maskleft);	   

}
	Mat show(g_frame);
    cv::imshow(window_name, show);

	// controls
unsigned char key = waitKey(10); // wait for key press for 10ms.
	
  if(key =='1'){
 mask_f=imread("avatars/caps.jpg");
 mask_left=imread("avatars/profile_left.jpg");
 text="Captain!"; 
  }
  else if(key =='2'){
 mask_f=imread("avatars/Iron_Man.jpg");
 mask_left=imread("avatars/Iron_man_p.jpg");
 text="Mr.Stark"; 
  } 

//   else if(key =='3'){
// mask_f=imread("avatars/.jpg");
// mask_left=imread("avatars/.jpg");
// text=""; 
//  } 

g_mask.upload(mask_f);
g_maskleft.upload(mask_left);
     
          if (key == 27) //wait for 'ESC' If 'ESC' key is pressed, break loop
       {
            cout << "esc key is pressed by user" << endl;
            break; 
       }
	}
return 0;
}


